package mongo_up_and_go

import (
	"encoding/hex"
	"encoding/json"
	"go.mongodb.org/mongo-driver/x/mongo/driver/uuid"
	"io"
	"reflect"
	"io/ioutil"
)

const (
	classError   = "ERROR"
	classSuccess = "SUCCESS"
)

type (
	UUID       string
	TypeFabric interface {
		Alloc() interface{}
		Release()
		PathId(string, *QueryMongoContext) (*QueryMongoContext, bool)
		GenerateId()
		RestoreId(*QueryMongoContext)
		Normalization(*QueryMongoContext)
		ExtractMe(string, []interface{}, func(interface{}) error) ([]interface{}, error)
	}
	SelectContext interface {
		GetSelectedId(objType TypeFabric) UUID
		GetSelectedIdByCollection(objCollection string) UUID
	}
)

func Key() (result UUID) {
	key, _ := uuid.New()
	return UUID(hex.EncodeToString(key[:]))
}

func KeyToStr(key UUID) string {
	return string(key)
}

func StrToKey(str string) (key UUID) {
	return UUID(str)
}

func readAndDecodeData(reader io.ReadCloser, destination interface{}) (fields []string) {
	data, err := ioutil.ReadAll(reader)
	if err != nil {
		panic(err)
	}
	var i interface{} = new(interface{})
	err = json.Unmarshal(data, i)
	if err != nil {
		panic(err)
	}
	err = json.Unmarshal(data, destination)
	if err != nil {
		panic(err)
	}
	val := reflect.ValueOf(i).Elem().Elem()
	keys := val.MapKeys()
	for _, kv := range keys {
		fields = append(fields, kv.String())
	}
	return
}

func isEmptyValue(v reflect.Value) bool {
	switch v.Kind() {
	case reflect.Array, reflect.Map, reflect.Slice, reflect.String:
		return v.Len() == 0
	case reflect.Bool:
		return !v.Bool()
	case reflect.Int, reflect.Int8, reflect.Int16, reflect.Int32, reflect.Int64:
		return v.Int() == 0
	case reflect.Uint, reflect.Uint8, reflect.Uint16, reflect.Uint32, reflect.Uint64, reflect.Uintptr:
		return v.Uint() == 0
	case reflect.Float32, reflect.Float64:
		return v.Float() == 0
	case reflect.Interface, reflect.Ptr:
		return v.IsNil()
	}
	return false
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}