package mongo_up_and_go

import (
	"fmt"
	"strings"
)

const (
	errorPath   = "PathError"
	errorConfig = "ConfigError"
)

type (
	internalServerError struct {
		Code    int
		Message string
		Class   string
	}
)

func (s *internalServerError) Error() string {
	return fmt.Sprintf("[%d] %s: %s", s.Code, s.Class, s.Message)
}

func (s *internalServerError) Meta() Metadata {
	return Metadata{
		Code:    s.Code,
		Class:   s.Class,
		Message: s.Message,
	}
}

func PathNotSupported(path []string) error {
	return &internalServerError{
		Code:    1001,
		Message: "unknown path at '..." + strings.Join(path, "/") + "'",
		Class:   errorPath,
	}
}

func MethodNotSupported(method string) error {
	return &internalServerError{
		Code:    1001,
		Message: "unsupported method '" + method + "'",
		Class:   errorPath,
	}
}

func WrongConfiguration(message string) error {
	return &internalServerError{
		Code:    1002,
		Message: message,
		Class:   errorConfig,
	}
}
