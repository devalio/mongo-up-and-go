package mongo_up_and_go

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"io/ioutil"
	"net/http"
	"sync"
	"testing"
	"time"
)

const (
	constPort         = "8110"
	collectionClasses = "Classes"
)

type (
	// Id document structure ----------------------------------------------------------------------------------------
	Rates struct {
		Id        UUID   `bson:"_id,omitempty" json:"_id,omitempty"`
		Objective string `bson:"objective,omitempty" json:"objective,omitempty"`
		Rate      int    `bson:"rate,omitempty" json:"rate,omitempty"`
	}
	Learner struct {
		Id     UUID    `bson:"_id,omitempty" json:"_id,omitempty"`
		Name   string  `bson:"name,omitempty" json:"name"`
		Sex    rune    `bson:"sex,omitempty" json:"sex"`
		Remand bool    `bson:"remand,omitempty" json:"remand"`
		Rates  []Rates `bson:"rates" json:"rates"`
	}
	Class struct {
		Id       UUID      `bson:"_id,omitempty" json:"_id,omitempty"`
		Year     int       `bson:"year,omitempty" json:"year"`
		Alpha    string    `bson:"alpha,omitempty" json:"alpha"`
		Learners []Learner `bson:"learners,omitempty" json:"learners"`
	}

	TreasurySelector struct {
		Class   UUID `json:"class"`
		Learner UUID `json:"learner"`
		Rate    UUID `json:"rate"`
	}
)

var siteMap = []PageEntity{
	{
		Path:        "classes",
		Collection:  collectionClasses,
		FieldName:   "",
		IdentifyBy:  "_id",
		ObjectType:  &Class{},
		UpsertAllow: false,
		Childs: []PageEntity{
			{
				Path:       "learners",
				Collection: collectionClasses,
				FieldName:  "learners",
				IdentifyBy: "_id",
				ObjectType: &Learner{},
				MethReplace: map[string]string{
					"POST":   "PUT",
					"DELETE": "PUT",
				},
				UpsertAllow: false,
				Childs: []PageEntity{
					{
						Path:       "rates",
						Collection: collectionClasses,
						FieldName:  "rates",
						IdentifyBy: "_id",
						ObjectType: &Rates{},
						MethReplace: map[string]string{
							"POST":   "PUT",
							"DELETE": "PUT",
						},
						UpsertAllow: false,
					},
				},
			},
		},
	},
}

// TreasurySelector ====================================================================================================

func (s *TreasurySelector) GetSelectedId(objType TypeFabric) UUID {
	switch objType.(type) {
	case *Class:
		return s.Class
	case *Learner:
		return s.Learner
	case *Rates:
		return s.Rate
	default:
		panic(WrongConfiguration(fmt.Sprintf("Cannot extract ID for %T collection", objType)))
	}
}

func (s *TreasurySelector) GetSelectedIdByCollection(objCollection string) UUID {
	if objCollection == collectionClasses {
		return s.Class
	} else {
		panic(WrongConfiguration(fmt.Sprintf("Cannot extract ID for %s collection", objCollection)))
	}
}

// Class ===============================================================================================================

func (_ *Class) Alloc() interface{} {
	return new(Class)
}

func (_ *Class) Release() {

}

func (c *Class) GenerateId() {
	c.Id = Key()
}

func (_ *Class) PathId(id string, q *QueryMongoContext) (*QueryMongoContext, bool) {
	q.Selected.(*TreasurySelector).Class = StrToKey(id)
	return q, true
}

func (c *Class) RestoreId(q *QueryMongoContext) {
	if KeyToStr(c.Id) == "" {
		if KeyToStr(q.Selected.(*TreasurySelector).Class) != "" {
			c.Id = q.Selected.(*TreasurySelector).Class
		} else {
			c.Id = Key()
		}
	}
}

func (c *Class) Normalization(q *QueryMongoContext) {
	if KeyToStr(c.Id) == "" {
		c.Id = Key()
	}
	for i := range c.Learners {
		if KeyToStr(c.Learners[i].Id) == "" {
			c.Learners[i].Id = Key()
		}
		for j := range c.Learners[i].Rates {
			if KeyToStr(c.Learners[i].Rates[j].Id) == "" {
				c.Learners[i].Rates[j].Id = Key()
			}
		}
	}
}

func (c *Class) ExtractMe(collection string, array []interface{}, decode func(i interface{}) error) ([]interface{}, error) {
	if collection == collectionClasses {
		err := decode(c)
		return append(array, c), err
	} else {
		panic(WrongConfiguration(fmt.Sprintf("The Class document cannot be extract from the %s collection", collection)))
	}
}

// Learner =============================================================================================================

func (_ *Learner) Alloc() interface{} {
	return new(Learner)
}

func (_ *Learner) Release() {

}

func (l *Learner) GenerateId() {
	l.Id = Key()
}

func (_ *Learner) PathId(id string, q *QueryMongoContext) (*QueryMongoContext, bool) {
	q.Selected.(*TreasurySelector).Learner = StrToKey(id)
	return q, true
}

func (l *Learner) RestoreId(q *QueryMongoContext) {
	if KeyToStr(l.Id) == "" {
		if KeyToStr(q.Selected.(*TreasurySelector).Learner) != "" {
			l.Id = q.Selected.(*TreasurySelector).Learner
		} else {
			l.Id = Key()
		}
	}
}

func (c *Learner) Normalization(q *QueryMongoContext) {
	if KeyToStr(c.Id) == "" {
		c.Id = Key()
	}
	for i := range c.Rates {
		if KeyToStr(c.Rates[i].Id) == "" {
			c.Rates[i].Id = Key()
		}
	}
}

func (l *Learner) ExtractMe(collection string, array []interface{}, decode func(i interface{}) error) ([]interface{}, error) {
	if collection == collectionClasses {
		var c Class
		err := decode(&c)
		for _, el := range c.Learners {
			array = append(array, el)
		}
		return array, err
	} else {
		panic(WrongConfiguration(fmt.Sprintf("The Learner document cannot be extract from the %s collection", collection)))
	}
}

// Rates ===============================================================================================================

func (_ *Rates) Alloc() interface{} {
	return new(Rates)
}

func (_ *Rates) Release() {

}

func (l *Rates) GenerateId() {
	l.Id = Key()
}

func (_ *Rates) PathId(id string, q *QueryMongoContext) (*QueryMongoContext, bool) {
	q.Selected.(*TreasurySelector).Rate = StrToKey(id)
	return q, true
}

func (l *Rates) RestoreId(q *QueryMongoContext) {
	if KeyToStr(l.Id) == "" {
		if KeyToStr(q.Selected.(*TreasurySelector).Rate) != "" {
			l.Id = q.Selected.(*TreasurySelector).Rate
		} else {
			l.Id = Key()
		}
	}
}

func (c *Rates) Normalization(q *QueryMongoContext) {
	if KeyToStr(c.Id) == "" {
		c.Id = Key()
	}
}

func (l *Rates) ExtractMe(collection string, array []interface{}, decode func(i interface{}) error) ([]interface{}, error) {
	if collection == collectionClasses {
		var c Class
		err := decode(&c)
		for _, el := range c.Learners {
			for _, rt := range el.Rates {
				array = append(array, rt)
			}
		}
		return array, err
	} else {
		panic(WrongConfiguration(fmt.Sprintf("The Rate document cannot be extract from the %s collection", collection)))
	}
}

// SERVER ==============================================================================================================

type (
	httpServer struct {
		mux sync.Mutex
		db  *mongo.Client
		ch  *chan bool
	}
)

func (s *httpServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/start" && r.Method == http.MethodGet {
		*s.ch <- true
		return
	}
	var (
		ctx    QueryMongoContext
		result struct {
			Data interface{} `json:"data"`
			Meta Metadata    `json:"meta"`
		}
	)

	ctx.Path = r.URL.Path
	ctx.Context = context.TODO()
	ctx.Selected = &TreasurySelector{}
	ctx.Method = r.Method
	ctx.Body = r.Body
	ctx.Mongo = s.db.Database("Treasurer")
	result.Data, result.Meta = DoExecuteEndpoint(&ctx, &siteMap)

	if outPage, err := json.Marshal(&result); err != nil {
		panic(err)
	} else {
		w.Write(outPage)
	}
}

func ConnectMongoDatabase() *mongo.Client {
	var c = "mongodb+srv://devalio:gfhjkm@cluster0-aum9v.gcp.mongodb.net/test?retryWrites=true"
	o := options.Client().ApplyURI(c)
	if err := o.Validate(); err != nil {
		panic(err)
	}
	if db, err := mongo.Connect(context.TODO(), o); err != nil {
		panic(err)
	} else {
		return db
	}
}

func startServer() {
	var (
		server httpServer
		ch     = make(chan bool, 1)
	)
	server.db = ConnectMongoDatabase()
	server.mux.Lock()
	server.ch = &ch

	go func() {
		if err := http.ListenAndServe(":"+constPort, &server); err != nil {
			panic(err)
		}
	}()
	go func() {
		time.Sleep(time.Millisecond * 100)
		url := "http://localhost:" + constPort + "/start"
		for {
			select {
			case <-*server.ch:
				server.mux.Unlock()
				return
			default:
				time.Sleep(time.Millisecond * 50)
				r, _ := http.NewRequest(http.MethodGet, url, nil)
				http.DefaultClient.Do(r)
			}
		}
	}()
	server.mux.Lock()
}

func CheckResponse(t *testing.T, data []byte) {
	println(string(data))
	var resp struct {
		Meta struct {
			Class   string `json:"class"`
			Code    int    `json:"code"`
			Message string `json:"message"`
		} `json:"meta"`
	}
	resp.Meta.Code = -1
	if err := json.Unmarshal(data, &resp); err != nil {
		t.Fatal(err)
	}
	if resp.Meta.Code != 0 {
		t.Errorf("Error code #%d with %s class: %s", resp.Meta.Code, resp.Meta.Class, resp.Meta.Message)
	}
}

var class4E = []Learner{
	{Key(), "Аванесян Нарек Арменович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Багдасарьян Милана Андраниковна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Банников Алексей Михайлович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Беловербенко Дарья Андреевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Белоусов Ярослав Антонович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Бокарева Валентина Андреевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Вашуркин Артур Романович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Бохан Аделина Алексеевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Воронина Мария Алексеевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Горлышева Алена Денисовна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Голубятниковна Ульяна Александровна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Дубинина София Андреевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Жуков Алексей Антонович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Зубихин Александр Александрович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Иваненков Владислав Андреевич", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Игнатенко Виктор Сергеевич", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Игнатенко Иван Сергеевич", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Касимова Фатима Асланова", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Каспари Вадим Артурович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Кашинцев Николай Алексеевич", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Козак Владимир Сергеевич", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Комаров Кирилл Денисович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Крылова Анастасия Григорьевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Красовская Мария Эдуардовна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Латонов Егор Александрович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Латошка София Андреевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Левашов Евгений Дмитриевич", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Маслова Александра Сергеевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Мелкопян Арсен Суренович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Меньшенина Алиса Игоревна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Орлова Алена Дмитриевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Расулова Зарина Асимовна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Родионов Богдан Фаятович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Саргсян Арен Арменович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Степанцова Алена Дмитриевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Теплова Анастасия Дмитриевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Уколова Анастасия Анатольевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Усачёва София Андреевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Харатян Илья Григорович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Хачатрян Мисак Ервандович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Хикматова Алина Азизбековна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Шедогуб Ярослав Александрович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
}

//1W
var class1W = []Learner{
	{Key(), "Адаменко Давид Сергеевич", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Акулин Егор Владимирович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Бородина Марианна Станистлавовна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Бочарова Анастасия Юрьевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Викман Валерия Алексеевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Горшков Илья Александрович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Горшков Никита Александрович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Горяинов Александр Александрович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Григорян Нодар Григорович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Давыдов Максим Анатольевич", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Дубинина Ксения Андреевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Еремеевская Алиса Даниловна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Еремеевский Елисей Данилович", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Еремеевская Таисия Даниловна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Закиров Бенемир Диловарович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Кудлич Вероника Сергеевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Кузнецова София Алексеевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Кужильный Константин Александрович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Марков Давид Юрьевич", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Миленина Надежда Сергеевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Морозов Владимир Александрович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Морозов Александр Александрович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Меньшенин Драгомир Игоревич", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Овчарук Полина Михайловна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Отливной Евгений Константинович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Овсепян Андреас Хачикович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Папикян Константин Суренович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Перетятькин Кирилл Сергеевич", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Реклинг Мария Андреевна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Сапожникова Анастасия Викторовна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Страшко Алексей Анатольевич", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Серик Герман Витальевич", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Серик Даниэль Витальевич", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Тутукина Диана Александровна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Хикматов Салим Азизбекович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Цветков Лев Геннадьевич", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Ципелева Эвелина Романовна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Чиковани Ливани Тенгизович", 'M', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
	{Key(), "Шамилова Азиза Абдуловна", 'F', false, []Rates{{Objective: "math", Rate: 5}, {Objective: "eng", Rate: 2}}},
}

func TestExecute(t *testing.T) {

	startServer()
	time.Sleep(1 * time.Second)

	var c Class

	c.Id = Key()
	c.Alpha = "E"
	c.Year = 2014
	c.Learners = class4E
	if data, err := json.Marshal(&c); err != nil {
		t.Fatal(err)
	} else {
		url := "http://localhost:" + constPort + "/classes"
		req, err := http.NewRequest("POST", url, bytes.NewReader(data))
		if err != nil {
			t.Fatal(err)
		}
		req.Header.Set("Content-Type", "application/json")
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatal(err)
		}
		if resp.StatusCode != 200 {
			t.Fatal(fmt.Sprintf("unexpected status %d", resp.StatusCode))
		}
		if body, err := ioutil.ReadAll(resp.Body); err != nil {
			t.Fatal(err)
		} else {
			println(string(body))
		}
	}

}

func TestGETClass(t *testing.T) {

	startServer()
	time.Sleep(1 * time.Second)
	classId := StrToKey("443e2769f4b94520844b0c8c3d0e12e6")

	url := "http://localhost:" + constPort + "/classes/" + KeyToStr(classId)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 200 {
		t.Fatal(fmt.Sprintf("unexpected status %d", resp.StatusCode))
	}
	if data, err := ioutil.ReadAll(resp.Body); err != nil {
		t.Fatal(err)
	} else {
		CheckResponse(t, data)
	}

}

func TestGETLearnerOnce(t *testing.T) {

	startServer()
	time.Sleep(1 * time.Second)
	classId := StrToKey("443e2769f4b94520844b0c8c3d0e12e6")
	learnerId := StrToKey("e09a7f182b164001b09f198b17c1e200")

	url := "http://localhost:" + constPort + "/classes/" + KeyToStr(classId) + "/learners/" + KeyToStr(learnerId)
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 200 {
		t.Fatal(fmt.Sprintf("unexpected status %d", resp.StatusCode))
	}
	if data, err := ioutil.ReadAll(resp.Body); err != nil {
		t.Fatal(err)
	} else {
		CheckResponse(t, data)
	}

}

func TestGETLearnersAll(t *testing.T) {

	startServer()
	time.Sleep(1 * time.Second)
	classId := StrToKey("443e2769f4b94520844b0c8c3d0e12e6")

	url := "http://localhost:" + constPort + "/classes/" + KeyToStr(classId) + "/learners"
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 200 {
		t.Fatal(fmt.Sprintf("unexpected status %d", resp.StatusCode))
	}
	if data, err := ioutil.ReadAll(resp.Body); err != nil {
		t.Fatal(err)
	} else {
		CheckResponse(t, data)
	}

}

func TestPost(t *testing.T) {

	startServer()
	time.Sleep(1 * time.Second)
	classId := StrToKey("443e2769f4b94520844b0c8c3d0e12e6")
	var p Learner
	p.Name = "Аванесян Чебурек Арменович"
	p.Remand = false
	p.Sex = 'M'

	data, err := json.Marshal(&p)
	if err != nil {
		t.Fatal(err)
	}

	url := "http://localhost:" + constPort + "/classes/" + KeyToStr(classId) + "/learners"
	req, err := http.NewRequest("POST", url, bytes.NewReader(data))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 200 {
		t.Fatal(fmt.Sprintf("unexpected status %d", resp.StatusCode))
	}
	if data, err := ioutil.ReadAll(resp.Body); err != nil {
		t.Fatal(err)
	} else {
		CheckResponse(t, data)
	}

}

func TestPUT(t *testing.T) {

	startServer()
	time.Sleep(1 * time.Second)
	classId := StrToKey("443e2769f4b94520844b0c8c3d0e12e6")
	personId := StrToKey("1c66b4f28a984a8983faa974d6a02e61")
	var p Learner
	p.Name = "Аванесян Чубурек Семенович"
	p.Remand = false
	p.Sex = 'M'
	data, err := json.Marshal(&p)
	if err != nil {
		t.Fatal(err)
	}

	url := "http://localhost:" + constPort + "/classes/" + KeyToStr(classId) + "/learners/" + KeyToStr(personId)
	req, err := http.NewRequest("PUT", url, bytes.NewReader(data))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 200 {
		t.Fatal(fmt.Sprintf("unexpected status %d", resp.StatusCode))
	}
	if data, err := ioutil.ReadAll(resp.Body); err != nil {
		t.Fatal(err)
	} else {
		CheckResponse(t, data)
	}

}

func TestPUTNew(t *testing.T) {

	startServer()
	time.Sleep(1 * time.Second)
	classId := StrToKey("443e2769f4b94520844b0c8c3d0e12e6")
	personId := Key()
	var p Learner
	p.Name = "Меньшенин Игорь Владимирович"
	p.Remand = false
	p.Sex = 'M'
	p.Id = personId

	data, err := json.Marshal(&p)
	if err != nil {
		t.Fatal(err)
	}

	url := "http://localhost:" + constPort + "/classes/" + KeyToStr(classId) + "/learners"
	req, err := http.NewRequest("PUT", url, bytes.NewReader(data))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 200 {
		t.Fatal(fmt.Sprintf("unexpected status %d", resp.StatusCode))
	}
	if data, err := ioutil.ReadAll(resp.Body); err != nil {
		t.Fatal(err)
	} else {
		CheckResponse(t, data)
	}

}

func TestPatch(t *testing.T) {

	startServer()
	time.Sleep(1 * time.Second)
	classId := StrToKey("443e2769f4b94520844b0c8c3d0e12e6")
	personId := StrToKey("eab2ac69b44d4a6fbbc0e854f7f1297d")

	type (
		Learner1 struct {
			Sex    rune `json:"sex"`
			Remand bool `json:"remand"`
		}
	)

	var p Learner1
	p.Sex = '0'
	p.Remand = false

	data, err := json.Marshal(&p)
	if err != nil {
		t.Fatal(err)
	}

	url := "http://localhost:" + constPort + "/classes/" + KeyToStr(classId) + "/learners/" + KeyToStr(personId)
	req, err := http.NewRequest("PATCH", url, bytes.NewReader(data))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 200 {
		t.Fatal(fmt.Sprintf("unexpected status %d", resp.StatusCode))
	}
	if data, err := ioutil.ReadAll(resp.Body); err != nil {
		t.Fatal(err)
	} else {
		CheckResponse(t, data)
	}

}

func TestDelete(t *testing.T) {

	startServer()
	time.Sleep(1 * time.Second)
	classId := StrToKey("443e2769f4b94520844b0c8c3d0e12e6")
	personId := StrToKey("1c66b4f28a984a8983faa974d6a02e61")

	url := "http://localhost:" + constPort + "/classes/" + KeyToStr(classId) + "/learners/" + KeyToStr(personId)
	req, err := http.NewRequest("DELETE", url, nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 200 {
		t.Fatal(fmt.Sprintf("unexpected status %d", resp.StatusCode))
	}
	if data, err := ioutil.ReadAll(resp.Body); err != nil {
		t.Fatal(err)
	} else {
		CheckResponse(t, data)
	}

}

func TestThreeLevelPatch(t *testing.T) {

	startServer()
	time.Sleep(1 * time.Second)
	classId := StrToKey("ff779733b1244a969fd1d0573568e403")
	personId := StrToKey("a260859de2254d52a64648a9091a3225")
	rateId := StrToKey("a86100a5900b4ed8a0a23c6502725047")

	type (
		Rate1 struct {
			Rate int `json:"rate"`
		}
	)

	var r = Rate1{10}

	data, err := json.Marshal(&r)
	if err != nil {
		t.Fatal(err)
	}

	url := "http://localhost:" + constPort + "/classes/" + KeyToStr(classId) + "/learners/" + KeyToStr(personId) + "/rates/" + KeyToStr(rateId)
	req, err := http.NewRequest("PATCH", url, bytes.NewReader(data))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != 200 {
		t.Fatal(fmt.Sprintf("unexpected status %d", resp.StatusCode))
	}
	if data, err := ioutil.ReadAll(resp.Body); err != nil {
		t.Fatal(err)
	} else {
		CheckResponse(t, data)
	}

}
