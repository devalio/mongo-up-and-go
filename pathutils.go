package mongo_up_and_go

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"io"
	"net/http"
	"strings"
)

type (
	QueryMongoContext struct {
		Collection string
		Context  context.Context
		Path     string
		Method   string
		Body     io.ReadCloser
		Mongo    *mongo.Database
		Selected SelectContext
		FieldsTree []PageEntity
	}
	Metadata struct {
		Class   string `json:"class"`
		Code    int    `json:"code"`
		Message string `json:"message"`
	}
	PageEntity struct {
		Path        string
		Collection  string
		FieldName   string
		IdentifyBy  string
		MethReplace map[string]string
		ObjectType  TypeFabric
		UpsertAllow bool
		Childs      []PageEntity
	}
)

func DoExecuteEndpoint(ctx *QueryMongoContext, apiMap *[]PageEntity) (data interface{}, meta Metadata) {

	defer func() {
		if r := recover(); r != nil {
			if srvErr, ok := r.(*internalServerError); ok {
				meta = srvErr.Meta()
			} else {
				meta.Message = fmt.Sprintf("<%T>: %s", r, r)
				meta.Class = classError
				meta.Code = 500
			}
			data = nil
		} else {
			if meta.Code == 0 {
				meta.Class = classSuccess
				meta.Message = "Ok"
			}
		}
	}()

	path := strings.Split(ctx.Path, "/")[1:]
	endPoint := routeEndpointPath(path, ctx, apiMap)
	data, meta = executeEndpoint(endPoint, ctx)
	return

}

func routeEndpointPath(path []string, ctx *QueryMongoContext, routeMap *[]PageEntity) *PageEntity {
	var point *PageEntity
	for i := range *routeMap {
		if (*routeMap)[i].Path == path[0] {
			point = &(*routeMap)[i]
			break
		}
	}
	if point == nil {
		panic(PathNotSupported(path))
	}
	if ctx.Collection != point.Collection {
		ctx.FieldsTree = make([]PageEntity, 0)
		ctx.Collection = point.Collection
	}
	path = path[1:]
	if len(path) > 0 && point.ObjectType != nil {
		var ok bool
		ctx, ok = point.ObjectType.PathId(path[0], ctx)
		if ok {
			if point.FieldName != "" {
				ctx.FieldsTree = append(ctx.FieldsTree, *point)
			}
			path = path[1:]
		}
	}
	if len(path) == 0 {
		return point
	} else {
		return routeEndpointPath(path, ctx, &point.Childs)
	}
}

func executeEndpoint(page *PageEntity, ctx *QueryMongoContext) (data interface{}, meta Metadata) {
	var methodName string
	if m, ok := page.MethReplace[ctx.Method]; ok {
		methodName = m
	} else {
		methodName = ctx.Method
	}
	collection := ctx.Mongo.Collection(ctx.Collection)
	switch methodName {
	case http.MethodGet:
		return executeGet(collection, page, ctx)
	case http.MethodPost:
		return executePost(collection, page, ctx)
	case http.MethodPut:
		return executePut(collection, page, ctx)
	case http.MethodPatch:
		return executePatch(collection, page, ctx)
	case http.MethodDelete:
		return executeDelete(collection, page, ctx)
	default:
		panic(MethodNotSupported(ctx.Method))
	}
}

func buildMetaField(err error) (meta Metadata, success bool) {
	if err != nil {
		success = false
		meta.Class = classError
		meta.Code = 1000
		meta.Message = fmt.Sprintf("<%T>: %s", err, err.Error())
	} else {
		success = true
		meta.Class = classSuccess
		meta.Code = 0
		meta.Message = "Ok"
	}
	return
}

func executeGet(collection *mongo.Collection, page *PageEntity, ctx *QueryMongoContext) (data interface{}, meta Metadata) {
	result, err := collection.Find(
		ctx.Context,
		pageFilterObject(page, ctx, nil),
		pageFindOptions(page, ctx),
	)
	var ok bool
	if meta, ok = buildMetaField(err); ok {
		defer result.Close(ctx.Context)
		data = readFromCursor(result, page, ctx)
	}
	return
}

func executePost(collection *mongo.Collection, page *PageEntity, ctx *QueryMongoContext) (data interface{}, meta Metadata) {
	obj := page.ObjectType.Alloc()
	defer obj.(TypeFabric).Release()
	readAndDecodeData(ctx.Body, obj)
	obj.(TypeFabric).Normalization(ctx)
	result, err := collection.InsertOne(ctx.Context, obj)
	meta, _ = buildMetaField(err)
	data = result
	return
}

func executePut(collection *mongo.Collection, page *PageEntity, ctx *QueryMongoContext) (data interface{}, meta Metadata) {
	obj := page.ObjectType.Alloc()
	defer obj.(TypeFabric).Release()
	if ctx.Method == http.MethodDelete {
		result, err := collection.UpdateOne(
			ctx.Context,
			pageFilterObject(page, ctx, obj),
			pageUpdateOperator(page, ctx, nil, obj),
			pageUpdateOption(page, ctx, obj),
		)
		meta, _ = buildMetaField(err)
		data = result
	} else {
		fields := readAndDecodeData(ctx.Body, obj)
		obj.(TypeFabric).RestoreId(ctx)
		obj.(TypeFabric).Normalization(ctx)
		result, err := collection.UpdateOne(
			ctx.Context,
			pageFilterObject(page, ctx, obj),
			pageUpdateOperator(page, ctx, fields, obj),
			pageUpdateOption(page, ctx, obj),
		)
		meta, _ = buildMetaField(err)
		data = result
	}
	return
}

func executePatch(collection *mongo.Collection, page *PageEntity, ctx *QueryMongoContext) (data interface{}, meta Metadata) {
	obj := page.ObjectType.Alloc()
	defer obj.(TypeFabric).Release()
	fields := readAndDecodeData(ctx.Body, obj)
	obj.(TypeFabric).RestoreId(ctx)
	obj.(TypeFabric).Normalization(ctx)
	result, err := collection.UpdateOne(
		ctx.Context,
		pageFilterObject(page, ctx, obj),
		pageUpdateOperator(page, ctx, fields, obj),
		pageUpdateOption(page, ctx, obj),
	)
	meta, _ = buildMetaField(err)
	data = result
	return
}

func executeDelete(collection *mongo.Collection, page *PageEntity, ctx *QueryMongoContext) (data interface{}, meta Metadata) {
	result, err := collection.DeleteOne(
		ctx.Context,
		pageFilterObject(page, ctx, nil),
		pageDeleteOption(page, ctx, nil),
	)
	meta, _ = buildMetaField(err)
	data = result
	return
}
