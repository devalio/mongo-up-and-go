package mongo_up_and_go

import (
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"net/http"
	"reflect"
	"strings"
)

func readFromCursor(cur *mongo.Cursor, page *PageEntity, ctx *QueryMongoContext) (result []interface{}) {
	elem := page.ObjectType.Alloc()
	defer elem.(TypeFabric).Release()
	var err error
	result = make([]interface{}, 0)
	for cur.Next(ctx.Context) {
		result, err = elem.(TypeFabric).ExtractMe(ctx.Collection, result, cur.Decode)
		if err != nil {
			panic(err)
		}
	}
	return
}

func pageFindOptions(p *PageEntity, ctx *QueryMongoContext) (optFind *options.FindOptions) {
	optFind = options.Find()
	optFind.SetLimit(100)
	optFind.SetShowRecordID(true)
	if p.FieldName != "" {
		id := ctx.Selected.GetSelectedId(p.ObjectType)
		if KeyToStr(id) != "" {
			optFind = optFind.SetProjection(bson.D{{p.FieldName + ".$", true}})
		}
	}
	return
}

func pageFilterObject(p *PageEntity, ctx *QueryMongoContext, _ interface{}) interface{} {
	collectionId := ctx.Selected.GetSelectedIdByCollection(ctx.Collection)
	entityId := ctx.Selected.GetSelectedId(p.ObjectType)
	if p.FieldName != "" {
		if ctx.Method == http.MethodPut || KeyToStr(entityId) == "" {
			return bson.D{{"_id", collectionId}}
		} else if ctx.Method == http.MethodDelete {
			return bson.D{{"_id", collectionId}}
		} else {
			// TODO: this may be reduced to "_id: ..."
			// return bson.M{"_id": collectionId, p.FieldName + "._id": entityId}
			return bson.D{{"_id", collectionId}}
		}
	} else {
		if ctx.Method == http.MethodGet {
			if KeyToStr(collectionId) != "" {
				return bson.D{{"_id", collectionId}}
			}
			return bson.D{}
		}
		if ctx.Method == http.MethodPut || ctx.Method == http.MethodDelete {
			if KeyToStr(collectionId) == "" {
				panic("you must specify a class id")
			}
			return bson.D{{"_id", collectionId}}
		}
	}
	return bson.D{}
}

func pageUpdateOperator(p *PageEntity, ctx *QueryMongoContext, fields []string, v interface{}) interface{} {
	entityId := ctx.Selected.GetSelectedId(p.ObjectType)
	if p.FieldName != "" {
		if ctx.Method == http.MethodPatch {

			if KeyToStr(entityId) == "" {
				return bson.D{
					{"$push", bson.D{{p.FieldName, v}}},
				}
			} else {

				var updateOp bson.D
				refData := reflect.ValueOf(v).Elem()
				for fldNum := 0; fldNum < refData.NumField(); fldNum++ {
					bsonName := reflect.TypeOf(v).Elem().Field(fldNum).Tag.Get("bson")
					if len(strings.Split(bsonName, ",")) > 1 {
						clearName := strings.TrimSpace(strings.Split(bsonName, ",")[0])
						flagOmitempty := strings.TrimSpace(strings.Split(bsonName, ",")[1])
						if flagOmitempty == "omitempty" {
							if !stringInSlice(clearName, fields) && (strings.Split(bsonName, ",")[0] == p.IdentifyBy || isEmptyValue(refData.Field(fldNum))) {
								clearName = ""
							}
						}
						bsonName = clearName
					}
					if bsonName != "" {
						fieldRef := ""
						for _, pathSegment := range ctx.FieldsTree {
							fieldRef += pathSegment.FieldName + ".$[" + pathSegment.FieldName + "]."
						}
						fieldRef += bsonName
						updateOp = append(updateOp, bson.E{Key: fieldRef, Value: refData.Field(fldNum).Interface()})
					}
				}
				return bson.D{{"$set", updateOp}}
			}

		} else
		if ctx.Method == http.MethodDelete {
			return bson.D{
				{"$pull", bson.D{{p.FieldName, bson.D{{"_id", entityId}}}}},
			}
		} else {
			if KeyToStr(entityId) != "" {
				return bson.D{
					{"$set", bson.D{{p.FieldName + ".$[" + p.FieldName + "]", v}}},
				}
			} else {
				return bson.D{
					{"$push", bson.D{{p.FieldName, v}}},
				}
			}
		}
	}
	return bson.D{{"$set", v}}
}

func pageUpdateOption(p *PageEntity, ctx *QueryMongoContext, _ interface{}) (optUpdate *options.UpdateOptions) {
	optUpdate = options.Update()
	entityId := ctx.Selected.GetSelectedId(p.ObjectType)
	optUpdate = optUpdate.SetUpsert(p.UpsertAllow)
	if p.FieldName != "" {
		if ctx.Method == http.MethodPatch {
			optUpdate = optUpdate.SetUpsert(false)
			if KeyToStr(entityId) != "" {
				var af options.ArrayFilters
				af.Filters = bson.A{}
				for _, pageEnt := range ctx.FieldsTree {
					entityId = ctx.Selected.GetSelectedId(pageEnt.ObjectType)
					af.Filters = append(af.Filters, bson.D{{pageEnt.FieldName + "." + pageEnt.IdentifyBy, entityId}})
				}
				optUpdate = optUpdate.SetArrayFilters(af)
			}
		} else
		if ctx.Method != http.MethodDelete {
			if KeyToStr(entityId) != "" {
				var af options.ArrayFilters
				af.Filters = bson.A{bson.D{{p.FieldName + "._id", entityId}}}
				optUpdate = optUpdate.SetArrayFilters(af)
			}
		}
	}
	return
}

func pageDeleteOption(_ *PageEntity, _ *QueryMongoContext, _ interface{}) (optDelete *options.DeleteOptions) {
	return options.Delete()
}
